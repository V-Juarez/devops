<h1>Jenkins</h1>

<h2>Yamil Asusta</h2>

<h1>Table Of Contents</h1>

- [1. Introducción](#1-introducción)
  - [Introducción a Automatización](#introducción-a-automatización)
- [2. Jenkins Core](#2-jenkins-core)
  - [Introducción a Jenkins](#introducción-a-jenkins)
  - [Instalación y Configuración Básica de Jenkins](#instalación-y-configuración-básica-de-jenkins)
  - [Manejo Básico de Usuarios](#manejo-básico-de-usuarios)
- [3. Jobs](#3-jobs)
  - [¿Qué es un Job?](#qué-es-un-job)
  - [Configuración de un Job](#configuración-de-un-job)
  - [¿Cómo Jenkins interactúa con su máquina local?](#cómo-jenkins-interactúa-con-su-máquina-local)
- [3. Plugins](#3-plugins)
  - [Jenkins y su ecosistema de Plugins](#jenkins-y-su-ecosistema-de-plugins)
  - [Cadenas de Jobs](#cadenas-de-jobs)
  - [Connectándonos a GitHub](#connectándonos-a-github)
- [4. Pipelines](#4-pipelines)
  - [¿Qué es un 'Pipeline'?](#qué-es-un-pipeline)
  - [¿Cómo puedo acelerar mi development de Pipelines?](#cómo-puedo-acelerar-mi-development-de-pipelines)
- [5. Slave](#5-slave)
  - [Introducción a Slaves](#introducción-a-slaves)
  - [Conectado un Slave](#conectado-un-slave)
- [6. Cierre](#6-cierre)
  - [Cierre del curso, introducción a DevOp](#cierre-del-curso-introducción-a-devop)


# 1. Introducción

## Introducción a Automatización

Automatización = Mayor productividad.

> Jenkins es un servidor de automatización open source escrito en Java.

Los competidores mas directos para Jenkins, en mi opinion, son:

- GitLab CI
- GitHub Actions
  Todo lo que puedes hacer con Jenkis lo puedes hacer con estas dos herramientas.

Y las tres abarcan muy bien la `metodologia continua`: CI/CD

> Automatizar ayuda mucho a tener más tiempo para hacer otras tareas prioritarias y mejorar la productividad de todo.

Un termino clave dentro de la *automatización* viene siendo **Integración continua (CI)**. Es el proceso de automatizar la compilación y la prueba de código cada vez que un miembro del equipo confirma cambios en el control de versiones. 

CI alienta a los desarrolladores a compartir su código y las pruebas unitarias fusionando sus cambios en un repositorio de control de versiones compartido después de cada pequeña tarea completada. 

La confirmación del código activa un sistema de compilación automatizado para obtener el código más reciente del repositorio compartido y para compilar, probar y validar la rama maestra completa (también conocida como troncal o principal).

# 2. Jenkins Core

## Introducción a Jenkins

**Jenkins** es open source y probablemente el software de automatización más usado de todos, escrito en Java y corre en JVM. Es muy conveniente al ser una herramienta **extensible** al tener un ecosistema de **plugins** que te permiten extenderlo, puedes escribir tus propios **plugins** con Java, pero ya la comunidad ha desarrollado un sinfín de ellos.

También nos permite escalar de manera horizontal y verticalmente, puede correr un sin número de trabajos concurrentemente en una sola máquina y si esa máquina no da abasto se le puede dar más recursos a **Jenkins**. O una máquina no es suficiente entonces **Jenkins** nos permite escalar horizontalmente con *““slaves””* y controlar varios nodos para que trabajen por él.

**Jenkins** siempre esta siendo innovado y teniendo actualizaciones de seguridad, esto es importante porque es el *target* más grande de seguridad de una empresa porque lo tiene todo.

Algo que **Jenkins** ha trabajado mucho en los últimos años es que puedes escribir tus *““jobs””* o unidades de trabajo en código. Nosotros queremos que nuestra automatización también sea programática, no solo los comando a ejecutar sino poder migrar nuestro trabajo a un nuevo **Jenkins** de manera reproducible. Han creado *Pipelines as code*

### Caracteristicas

- Es Open Source
- Escrito en java sobre JVM
- Mayormente se corre en Linux
- Es extensible
- Se puede escribir plugins en java
- La comunidad aporta tanto escribiendo un sin fin de Plugins.
- Jenkins es amigable y flexible por la cantidad de comunidad que tiene.
- Compañias enormes tiene un solo jenkins
- Jenkins puede crecer horizontalmente añadiendo mas hadware ya que soporta esto con el termino **slaves**.
- Jenkins es un servicio de automatización.
- CircleCI realiza lo mismo que jenkins.
- Actualmente se puede escribir tu job con codigo y no solo con la interfaz.

>  **Escalabilidad Vertical ->** Relacionado con el hardware del servidor, lo conseguimos aumentando los recursos del servidor (procesamiento, memoria, almacenamiento).
>
> **Escalabilidad Horizontal ->** Agregar nodos adicionales para adaptarse a la carga de trabajo. Si la aplicación o el sistema están llegando a su punto crítico, entonces se agregan nodos adicionales y se divide la carga entre los distintos nodos. En Jenkis, lo podemos conseguir mediante “Slaves”.

**Jenkins** es open source y probablemente el software de automatización más usado de todos, escrito en Java y corre en JVM. 

**Jenkins** es muy conveniente. **Jenkins** es una herramienta extensible; esto quiere decir que tiene un **ecosistema de plugins** que te permiten extenderlo. Si quieres, *puedes escribir tus propios plugins con Java*, pero ya la comunidad ha desarrollado un sinfín de ellos💪🏻

## Instalación y Configuración Básica de Jenkins

 maquina local es en un folder crear otro folder llamado jenkins_home y al lado un docker-compose.yml con el siguiente codigo

```yml
version: '3'
services:
  jenkins:
    container_name: jenkins
    image: jenkins/jenkins
    ports:
      - "8080:8080"
    volumes:
      - $PWD/jenkins_home:/var/jenkins_home
    networks:
      - net
networks:
  net:
```

Despues le dan en la consola a docker-compose up -d, ahora con entrar al localhost con ese puerto.

Una instalación más básica y sencilla que propone la doc de Jenkins es descargando el .war:

1. Download Jenkins (el .war).
2. Open up a terminal in the download directory.
3. Run java `-jar jenkins.war --httpPort=8080`
4. Browse to [http://localhost:8080](http://localhost:8080/)

Claro que para que funcione debe estar instalado java en sus maquinas [pipeline](https://jenkins.io/doc/pipeline/tour/getting-started/)

### Arch

Jenkins

```sh
sudo pacman -S jenkins
```

Inicializar jenkins

```sh
# ejecutar comando como root
systemctl enable jenkins.services
```

Acticar Jenkins

```sh
# root
sytemctl start jenkins.services
```

password

```sh
# cat al directorio password
sudo cat /var/lib/jenkins/secrets/initialAdminPassword

# password
sadfaksjdhfajklshfalskjdhfajkhflakhsdfjkashdfkjahskjfhasldhfaif
```

visitar la siguiente url

[http://localhost:809](http://localhost:8090/)

Pegas la contrasena.

![Screenshot_2.png](https://static.platzi.com/media/user_upload/Screenshot_2-4aced133-ab81-44ce-a45e-f0282cf4426a.jpg)

elegir la opcion 1.

### Instalación de Jenkins con Docker en Linux para que solo copies y pegues

#### Prerrequisitos:

\- mínimo recomendado 1GB de RAM
\- mínimo recomendado 50GB espacio en disco
\- Java 8 runtime
\- Docker Engine

#### Instrucciones:

1.- Crea un archivo Dockerfile

```sh
touch Dockerfile
```

2.- agrega las siguientes líneas:

```dockerfile
FROM jenkins/jenkins:2.263.1-lts-slim
USER root
RUN apt-get update && apt-get install -y apt-transport-https
ca-certificates curl gnupg2
software-properties-common
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
RUN apt-key fingerprint 0EBFCD88
RUN add-apt-repository
"deb [arch=amd64] https://download.docker.com/linux/debian
$(lsb_release -cs) stable"
RUN apt-get update && apt-get install -y docker-ce-cli
USER jenkins
RUN jenkins-plugin-cli --plugins blueocean:1.24.3
```

3.- Construye una imagen de docker con este archivo (no olvides el punto al final del comando ya que le indica que en la carpeta actual encontrará el archivo Dockerfile)

```sh
sudo docker build -t myjenkins-blueocean:1.1
```

***Nota:** myjenkins-blueocean:1.1 es el nombre que desees ponerle a tu contenedor y su versión.

4.- Finalmente ejecuta el contenedor con el siguiente comando

```sh
sudo docker run --name jenkins-blueocean --rm --detach \
  --network jenkins --env DOCKER_HOST=tcp://docker:2376 \
  --env DOCKER_CERT_PATH=/certs/client --env DOCKER_TLS_VERIFY=1 \
  --publish 8080:8080 --publish 50000:50000 \
  --volume jenkins-data:/var/jenkins_home \
  --volume jenkins-docker-certs:/certs/client:ro \
  myjenkins-blueocean:1.1
```

ahora puedes acceder a http:/localhost:8080

![jenkins.PNG](https://static.platzi.com/media/user_upload/jenkins-01d20d20-4194-4030-944e-ae495b29f321.jpg)

Para obtener la llave de acceso primero deberás obtener el nombre o id del contenedor con el siguiente comando

```sh
sudo docker ps
```

cópialo y a continuación ejecuta el siguiente comando modificando “***id_contenedor***” por el id que obtuviste

```sh
sudo docker exec id_ contenedor cat var/Jenkins_home/secrets/initialAdminPassword
```

[![img](https://www.google.com/s2/favicons?domain=https://jenkins.io/sites/default/files/jenkins_favicon.ico)Jenkins User Documentation](https://jenkins.io/doc/)

## Manejo Básico de Usuarios

- Se pueden crear nuevos usuarios y asignarles diferentes permisos, esto con el fin de poder saber en todo momento, o auditorías quien hizo que…
- Lo ideal es no compartir mismos usuarios ni misma contraseña.
- La autenticación se puede dar por medio de login con Github o Google, esto con el uso de plugins.

**Para crear, eliminar, editar un usuario:**
Ir a Manage Jenkins/ Manage Users/ Create user (en caso de crear). Para editar o borrar solo se debe dar clic en user id deseado, después elegir opción *Configurar* o *Borrar*.

> r auditoria de todo lo que se haga…
> +1 para el jumpcloud de LDAP as a service, lo probare

> El PlugIn para registrasce con la cuenta de Google es el siguiente:
> Google Login
>
> Allows you to log in to Jenkins with a Google account and restrict access to a Google Apps Domain.

[Plugin oficial de Google Cloud Platform](https://github.com/jenkinsci/google-login-plugin)

[Jenkins con Github](https://www.jenkins.io/solutions/github/)

[GitLab y Jenkins](https://docs.gitlab.com/ee/integration/jenkins.html)

# 3. Jobs

## ¿Qué es un Job?

En esta clase veremos la unidad más importante de Jenkins, los trabajos(**Jobs**) que ejecuta, puede correr varios de estos a la vez y es controlado por el **Build Executor**.

Podemos tener **Jobs** de diferentes tipos como Freestyle project, Pipeline, folder, Multi-configuration project, etc.

Cada vez que ocurre una ejecución de un **Job** se añade un numero al **Build History** y sirve para tener auditorias de cuál trabajo fue el último success o fail.

La parte más importante de Jenkins, los Jobs (los trabajos que ejecuta).
Jenkins puede hacer varios trabajos al tiempo, esto es controlado por el Build Executor.

Por cada job, Jenkins crea un folder dentro de su workspace `(/var/lib/jenkins/workspace/)`. Un build es una ejecución de un Job. Cada job tiene su Build History.

> - Builds son cada ejecución de un job.

## Configuración de un Job

**Windows**
Que la ejecución o invocación de un parámetro es : echo%NOMBRE_VARIABLE%

```sh
echo "%NAME% esta es la version de Java"
java -version
echo "%NAME% esta es la version de Node"
node -v
```

- *Descripcion:* ayuda a resolver cuando tienes un monton de jobs para describir.
- *Discard old builds:* ayuda a resolver cuando muchas cosas se llenan en tu disco duro
  *Days to keep builds:* 365 dias --> quiero tener este build por un año
  *Max # of builds to keep:* 2 —> guardar los ultimos dos builds
- *This project is parameterized:* Le puedes pasar parametros al build
  Add Parameter -> String Parameter
  Name: NAME
  Default Value: Boris Vargas
  Description: Descripcion
- *Disable this project:* sumamente importante, si algo sale mal en un job y quieres que nadie lo corra (La mayor parte de jobs corren automaticos)

**Source Code Management**
*Git:* Añadir un repositorio
*Credentials:* Credenciales
(Usaremos un script para ejecutar esta parte)

**Build Triggers**
(Estuvimos ejecutando a mano)
*Trigger builds remotely (e.g., from scripts):* Tienes para correrlo por una API
*Build after other projects are built:* Si termino de ejecutar job A quiero correr job B, unicamente si job A fue estable.
*Build periodically:* Acepta la sintaxis de un CRON jobs (Corre cada minuto cada X dia, ‘si queremos que algo se ejecute sabado en la noche me corres este JOB’)
*GitHub hook trigger for GITScm polling:* Vamos usar futuramente, cuando tengamos un push en Github el job se va ejecutar

**Build Environment**
*Delete workspace before build starts:* (Importante que lo marquen) si tu corres tu job y modificas tu job y dejas files (algo) en la proxima ejecucion va estar. Queremos que el subfolder este limpio.
*Use secret text(s) or file(s):* Para añadir secretos

**Bindings**
Llaves o variables de entorno o algo que no deberia estar expuesto a otros usuarios te permite guardarlo y accesarlo atraves de script.

- *Abort the build if it’s stuck:* Si el job que va a correr toda su vida, porque paso algo. (Si el job fallo o el S.O. fallo)
  *Timeout minutes:* 3 --> Si paso 3 minutos que cancele el build y falle (Poner como una variable global por comando)
- *Add timestamps to the Console Output:* Marcar para ver el tiempo de ejecucion en consola

**Build**
Command: echo “Hello platzi $NAME”

- *Run with timeout:* Si un comando demora mas, si un comando tarda demasiado le permites una ventaja mas de tiempo
- *Archive the artifacts:* Vamos a usar en el futuro, watch others jobs y que se ejecute.

## ¿Cómo Jenkins interactúa con su máquina local?

node + jenkins para el que lo esté haciendo el curso con docker:

```dockerfile
FROM jenkins/jenkins

USER root

RUN apt-get install -y curl \
  && curl -sL https://deb.nodesource.com/setup_11.x | bash - \
  && apt-get install -y nodejs \
  && curl -L https://www.npmjs.com/install.sh | sh

USER jenkins
```

###  Dockerfile y el docker-compose que cree para el curso.

Contiene node, npm, angular

## 

#### [Repo DockerHub dev](https://hub.docker.com/r/devopsggq/jenkins)

**devopsggq/jenkins:latest**

**Dockerfile**

```dockerfile
FROM jenkinsci/jenkins:lts
USER root
RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install -y git
RUN apt-get install -y curl \
    && curl -sL https://deb.nodesource.com/setup_14.x | bash - \
    && apt-get install -y nodejs \
    && curl -L https://www.npmjs.com/install.sh | sh \
    && npm install -g @angular/cli@latest

USER jenkins
```

**Docker-Compose**

```dockerfile
version: '3'

services:
  jenkins:
    image: devopsggq/jenkins:latest
    container_name: jenkins-ci
    privileged: true
    user: root
    ports: 
      - 7001:8080
      - 50000:50000
    volumes: 
      - ./jenkins_home/:/var/jenkins_home
      - ./jenkins_var/var/run/docker.sock:/var/run/docker.sock
      - ./jenkins_usr/usr/local/bin/docker:/usr/local/bin/docker
```

```sh
sudo apt-get update
sudo apt-get install curl
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
node --version
```

- Podemos instalar cualquier otro lenguaje (PHP. PYTHON, ETC)
- Jenkins trabaja con lo que tenga en la maquina instalado.
- Jenkins puede instalar y guardarlo en el File System.
- Jenkins puede hacer instalaciones el mismo.

> Es mejor instalarlo desde los plugins de jenkins para poder manejar multiples.

# 3. Plugins

## Jenkins y su ecosistema de Plugins

Una de las razones por la cual Jenkins es adorado es porque tiene **Plugins** para una mayoría de cosas.

Los **plugins** son unidades que extiende a Jenkins, después de instalarlo nuestra herramienta puede hacer algo nuevo, es recomendado instalarlos con la opción de ““Download now and Install after restart”” y así **Jenkins** se va a encargar de ejecutar todos los **Jobs** que estaban corriendo y cuando eso termine, lo va a instalar.

error

```sh
Error : java.security.InvalidAlgorithmParameterException: the trustAnchors parameter must be non-empty
```

la solución es

```sh
sudo /var/lib/dpkg/info/ca-certificates-java.postinst configure
```

**Plugins:** Son unidades que extienden a Jenkins
**Download now and install after restart**

- Recomendado
- Jenkins espera a que terminen todos los jobs
- luego hace las instalaciones
- Reinicia la máquina
- No acepta otros trabajos hasta que esté de nuevo disponible

[Jenkins Plugins](https://plugins.jenkins.io/)

## Cadenas de Jobs

Primero instalamos el plugin Parameterized Trigger, igual cómo instalamos anteriormente y reiniciamos.

**Luego vamos a crear 2 jobs nuevos:**

**watchers**: En este job, vamos a configure y vamos a “Build after other projects are built” y escribimos y escribimos hello-platzi, sí hello-platzi es successful, quiero que se ejecute watchers.

Y en la parte de executed shell, escribimos : echo “Running after hello-platzi success” y guardamos.

**parameterized**: Acepta parámetros cuando lo llamo. Marcamos la opción “ This project is parameterized” y en el name escribimos ROOT_ID.
Y en el execute shell: echo “calle with $ROOT_ID” y guardamos.

Y en hello-platzi, en Downstream project, y estos se añaden cuando jenkins se da cuenta que su job tiene una dependencia con otro.
Vamos al configure de hello-platzi y en el execute shell escribimos:

```sh
echo “Hello Platzi from $NAME”
Y añadir un build step que se llama : “Trigger/call build on other projects”, y en projects to build escribimos parameterized y le damos en añadir parámetros, luego parámetros predefinidos y escribimos:
ROOT_ID=$BUILD_NUMBER
```

BUILD_NUMBER es una variable de entorno, que es el valor de esta ejecución y guardamos.

Le damos en “**build with parameters**” y entramos al console output de parameterized y vemos que la ejecución número tal, fue la que ejecutó a parameterized.
Corre hello-platzi, él llama declarativamente a parameterized e indirectamente a watchers.

Corre los test para esta versión, cuando acabes, mandame esta versión a producción le pasó el id del commit, y se lo pasó a mí job que hace deployment y cuando lo resuelvas me lo despliegas.

El sabe la cadena de ejecuciones que tuvo, y cuál fue el que inició este proceso.
El profe recomienda usar parameterized jobs en vez de watchers, porque cuando uso watchers solo tengo tres opciones mientras que con parameterized jobs tengo más opciones.

## Connectándonos a GitHub

Es posible conectar un repositorio **de** GitHub a Jenkins para **que** cada vez **que** exista un push **se** haga un build del source code. Para **que** esto sea posible debemos realizar cambios tanto **en** Jenkins como **en** GitHub. 

**En** Jenkins: 

1. Debemos tener el GitHub **plugin** instalado 

2. Al crear el Job, debemos marcar el SCM **la** opció**n** **de** Git, y pegar **la** URL del repo. (Para esta acció**n** el host **de** Jenkins debe tener instalado Git) 

3. **En** el apartado "branches to build" si dejamos **en** blanco tomara **en** cuenta cualquier branch.  

4. **En** "Build Triggers" debemos marcar **la** opció**n** "GitHub hook trigger for GITScm polling" **En** GitHub: 

   1. Vamos al repo **de** GitHub. 
   2. Entramos **en** Settings -> Webhooks. 
   3.  Añadimos un nuevo Webhook. 
   4.  Añadimos **la** Payload URL. (Si **la** URL **no** acaba **en** /github-webhook/ GItHub lanzara un **error**.) 
   5. Marcar "Just the push event"

   **Local**

   Si lo estan corriendo local - localhost:8080

   Primero deben instalar ngrok es super facil y rapido.

   luego de instalarlo

   linux

   ```
   ./ngrok http 8080
   ```

   windows

   ```
   ngrok http 8080
   ```

   El les va a dar una url

   ```
   http--://[url_ngrok]/github-webhook/
   ```

**Local mente**

 Hacer la prueba use [ngrok.com](https://ngrok.com/) deben crear una cuenta descargar el archivo para su sistema, descomprimirlo y se autentican ./ngrok authtoken SU TOKEN y ejecutarlo ./ngrok http 8080
en Github en Webhooks agregan la url que des dio la consola usen la https:
Payload URL
https://SU_URL.ngrok.io/github-webhook/

Despues hacen cambios en el repositorio agregan el commit, push y la magia se hace.

Mi repositorio forked https://github.com/jhonsu01/platzi-scripts/

Success

![img](https://i.imgur.com/pF8NjXm.png)
![img](https://i.imgur.com/MsieZZ4.png)

# 4. Pipelines

## ¿Qué es un 'Pipeline'?

Pipelines nos permiten configurar nuestros Jobs con código en lugar de hacerlo en la interfaz visual. En Jenkins los hay de dos maneras: Scripting y Declarative.

>  ¿Cómo seria si uso uno privado de Bitbucket?
>
> En ese caso debes pasar los parámetros de las credenciales y si deseas el branch de la siguiente forma dentro del step:
> git ([url:‘https://github.com/autor/repo.git’, branch: ‘master’,credentialsId: ‘github’ ])
>
> donde credentialsId : ‘github’ es el nombre que le diste a tus credenciales dentro de Jenkins

[![img](https://www.google.com/s2/favicons?domain=https://jenkins.io/sites/default/files/jenkins_favicon.ico)Pipeline](https://jenkins.io/doc/book/pipeline/)

## ¿Cómo puedo acelerar mi development de Pipelines?

![Configurando Pipeline.png](https://static.platzi.com/media/user_upload/Configurando%20Pipeline-183eaaec-a87a-42af-bcf1-32eb0300b790.jpg)

![What is Pipeline .png](https://static.platzi.com/media/user_upload/What%20is%20Pipeline%20-0c863b9c-e0db-4bb5-b718-d40176f437d0.jpg)

Trabajando con jenkins desde un container de docker debe quitar el tools del Jenkinsfile para que le funcione. Quedando de esta manera:

```yml
pipeline {
  agent any
  options {
    timeout(time: 3, unit: 'MINUTES')
  }

  stages {
    stage('Install dependencies') {
      steps {
        sh 'cd jenkins-tests && npm install'
      }
    }
    stage('Run tests') {
      steps {
        sh 'cd jenkins-tests && npm test'
      }
    }
  }
}
```

[CI-Jenkins](https://github.com/V-Juarez/platzi-scripts.git)

# 5. Slave

## Introducción a Slaves

![Slaves.png](https://static.platzi.com/media/user_upload/Slaves-0f15095f-fe29-4576-ad48-439a5d5dcc5a.jpg)

Los Slaves nos permiten correr Jobs distribuidamente. Se conecta al Jenkins master y este le delega trabajos al Slave como si fuese otra máquina puede ser virtual, física como quieras hacerlo, nos permite escalar horizontalmente.

Jenkins permite ejecutar jobs distribuidamente
Ejecutar varios jobs en paralelo en máster

**En Jenkins Puede haber limitaciones:**

> Tamaño del job Recursos de la CPU Capacidad de Jenkins

**Slave:**

> Jenkins le delega trabajo al slave Permite escalar horizontalmente Cada Slave se puede ajustar a las necesidades de ejecución Se puede tener múltiples slaves asignados a un workload.

[Distributed builds - Jenkins](https://wiki.jenkins.io/display/JENKINS/Distributed+builds)

[Guia de Jenkins en PDF](https://www.jenkins.io/user-handbook.pdf)

## Conectado un Slave

![configurando un Slaves.png](https://static.platzi.com/media/user_upload/configurando%20un%20Slaves-4eecf814-e22b-4707-a2a9-3455b2a2b812.jpg)

```sh
cat .ssh/id_rsa.pub
copy key
adduser jenkins
apt-getupdate
apt-get install openjdk-8-jdk wget gnupg git vim
mkdir /var/jenkins/
chown jenkins:jenkins /var/jenkins
sudo su jenkins
cd 
mkdir .ssh
vim .ssh/authorized_keys
paste key
```

**Preparate jenkins slave**

`install.sh`

```sh
adduser jenkins
apt-get update
apt-get install openjdk-8-jdk wget gnupg git vim
mkdir /jenkins
chwon -R jenkins:jenkins /jenkins
# sudo su jenkins
# vim /home/jenkins/.ssh/authorized_keys
```

> Asi que tuve que darle permisos necesarios con los siguientes comandos:
>
> ```sh
> sudo chown -R jenkins:jenkins .ssh
> chmod 700 .ssh
> ```

![Conectando un Slaves.png](https://static.platzi.com/media/user_upload/Conectando%20un%20Slaves-c007d23d-43d0-406b-9d7e-46f368a27cda.jpg)

# 6. Cierre

## Cierre del curso, introducción a DevOp

>  Un dato importante es que para migrar jobs de un jenkins a otro (de una maquina a otra ) solo es cuestión de copiar los directorios del sistema operativo donde se encuentran ubicados los jobs , pegarlos en la nueva instancia de jenkins y listo!.