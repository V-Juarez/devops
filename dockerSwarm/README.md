# Docker Swarm
- [Docker Swarm](#docker-swarm)
- [1. Docker Swarm](#1-docker-swarm)
  - [Escala](#escala)
  - [Arquitectura Docker Swarm](#arquitectura-docker-swarm)
  - [Creadon aplicaiones para Docker Swarm: 12 Factores](#creadon-aplicaiones-para-docker-swarm-12-factores)
- [2. Primeros Pasos](#2-primeros-pasos)
  - [Instalar Docker](#instalar-docker)


# 1. Docker Swarm

> Internet -> Load Balancer -> Server1, server2, server3  Dev

## Escala

- [x] Vertical
- [x] Horizontal -> recomendado

## Arquitectura Docker Swarm

> El swarn de docker es un cluster muchas máquinas que están conectadas entre sí en red.
>
> Swarn **administrar** la comunicación entre ellas para que sean una unidad homogénea en la cual podamos correr aplicaciones.

## Creadon aplicaiones para Docker Swarm: 12 Factores

**¿ Está tu aplicación preparada para Docker Swarm ? **
Para saberlo, necesitas comprobarlo con los 12 factores

1. Codebase : el código debe estar en un repositorio
2. Dependencies : deben estar declaradas en un archivo de formato versionable, suele ser un archivo de código
3. Configuration : debe formar parte de la aplicación cuando esté corriendo, puede ser dentro de un archivo
4. Backing services : debe estar conectada a tu aplicación sin que esté dentro, se debe tratar como algo externo
5. Build, release, run : deben estar separadas entre sí.
6. Processes : todos los procesos los puede hacer como una unidad atómica
7. Port binding : tu aplicación debe poder exponerse a sí misma sin necesidad de algo intermediario
8. Concurrency : que pueda correr con múltiples instancias en paralelo
9. Disposabilty : debe estar diseñada para que sea fácilmente destruible
10. Dev/Prod parity : lograr que tu aplicación sea lo más parecido a lo que estará en producción
11. Logs : todos los logs deben tratarse como flujos de bytes
12. Admin processes : la aplicación tiene que poder ser ejecutable como procesos independientes de la aplicación


# 2. Primeros Pasos

## Instalar Docker

1. Descargar docker para su sistema operativo, macOS, windows, linux.

```sh
docker
```

2. Docker Linu

```sh
curl -fsSL https://get.docker.com | sudo sh 
# visitar docke install del sitio web para instalar docker
```